/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#ifndef TAUANALYSISTOOLS_SHAREDFILESVERSION_H
#define TAUANALYSISTOOLS_SHAREDFILESVERSION_H

/// Version of the calibration files
static const char *const sSharedFilesVersion = "00-03-16";

#endif // TAUANALYSISTOOLS_SHAREDFILESVERSION_H
